const firstName = document.getElementById("firstName");
const lastName = document.getElementById("lastName");
const submitButton = document.querySelector("#submit-button");
const totalScore = document.querySelector("#total-score");

const correctAnswers = {
      "number-1": "wolf",
      "number-2": "boy"
    };


submitButton.addEventListener("click", () => {
  let score = 0;
   	for (const [questionNum, correct ] of Object.entries(correctAnswers)) {
    const selectedAnswer = document.querySelector(`#${questionNum}`).value;
    const messageElement = document.querySelector(`#${questionNum}-text`);
    
    if (selectedAnswer === correct) {
      messageElement.innerText = "Correct!";
      messageElement.style.color = "green";
      score++;
    } else {
      messageElement.innerText = "Incorrect, please try again.";
      messageElement.style.color = "red";
    }
  }

  totalScore.innerText = `Good day, ${fName.value} ${lastName.value},You have ${score} out of ${Object.keys(correctAnswers).length}.`;

  alert(totalScore.innerText);
});